package com.grupo8.mydoctorhero.presenter;

import com.grupo8.mydoctorhero.model.DoctorRegistryInteractor;
import com.grupo8.mydoctorhero.mvp.HealthRegMVP;

public class HealthRegPresenter implements HealthRegMVP.Presenter {

    private  HealthRegMVP.View view;
    private  HealthRegMVP.Model model;

    public HealthRegPresenter(HealthRegMVP.View view){
        this.view = view;
        this.model = new DoctorRegistryInteractor();
    }

    @Override
    public void completeDoctorRegistration() {
        view.openFinalProfile();
    }
}
