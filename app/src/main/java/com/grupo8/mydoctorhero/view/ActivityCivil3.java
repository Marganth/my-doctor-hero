package com.grupo8.mydoctorhero.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;


import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.grupo8.mydoctorhero.R;


public class ActivityCivil3 extends AppCompatActivity {

    AppBarLayout tilAppBar3;
    MaterialToolbar appBar3;
    TextView txtLlegadaDocHero;
    Button btnCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civil3);

        initUI();
    }

    private void initUI() {

        tilAppBar3 = findViewById(R.id.til_app_bar3);
        appBar3 = findViewById(R.id.app_bar3);
        appBar3.setOnClickListener(evt ->onSettingsClick3());

        txtLlegadaDocHero = findViewById(R.id.txt_llegada_doc_hero);

        btnCancelar = findViewById(R.id.btn_cancelar);
        btnCancelar.setOnClickListener(evt ->onCancelarClick());
    }
    private void onSettingsClick3(){
        Intent intent = new Intent(this, ActivityCivil2.class);
        startActivity(intent);
    }

    private void onCancelarClick(){
        Intent intent = new Intent(this, ActivityCivil4.class);
        startActivity(intent);
    }
}