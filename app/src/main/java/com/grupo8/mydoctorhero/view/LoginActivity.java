package com.grupo8.mydoctorhero.view;

import static com.grupo8.mydoctorhero.R.layout.activity_login_activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;


import com.google.android.material.chip.Chip;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.grupo8.mydoctorhero.R;
import com.grupo8.mydoctorhero.mvp.LoginMVP;
import com.grupo8.mydoctorhero.presenter.LoginPresenter;


public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private LinearProgressIndicator progressLogin;

    private Chip btnSignin;
    private Chip btnLogin;

    private Chip btnFacebook;
    private Chip btnGmail;

    private TextInputLayout tilEmail;
    private TextInputLayout tilPasssword;

    private TextInputEditText etEmail;
    private TextInputEditText etPassword;

    private LoginMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_login_activity);



        presenter= new LoginPresenter(this);
        initUi();
    }

    private void initUi(){
        progressLogin = findViewById(R.id.progressLogin);

        tilEmail=findViewById(R.id.til_email);
        etEmail= findViewById(R.id.et_email);

        tilPasssword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnSignin = findViewById(R.id.btnSignin);
        btnSignin.setOnClickListener(v -> presenter.signin());

        btnLogin  = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(v -> presenter.loginWithEmail());

    }
    /*public void openChoiceActivity(){
        Intent intent = new Intent(this, ChoiceActivity.class);
        startActivity(intent);
    }

    public void openProfile(){
        Intent intent = new Intent(this, Activity_Profesional_de_la_Salud1.class);
        startActivity(intent);
    }*/

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(etEmail.getText().toString(), etPassword.getText().toString());
    }

    @Override
    public void showEmailerror(String error) {
        tilEmail.setError(error);

    }

    @Override
    public void showPassworderror(String error) {
        tilPasssword.setError(error);

    }

    @Override
    public void showGeneralMessage(String error) {
        Toast.makeText( LoginActivity.this, error, Toast.LENGTH_LONG).show();

    }

    @Override
    public void clearData() {
        tilEmail.setError("");
        etEmail.setText("");
        tilPasssword.setError("");
        etPassword.setError("");

    }

    @Override
    public void openNewActivity() {
        Intent intent = new Intent(this, Activity_Profesional_de_la_Salud1.class);
        startActivity(intent);

    }

    @Override
    public void openSignin(){
        Intent intent = new Intent(this, ChoiceActivity.class);
        startActivity(intent);
    }

    @Override
    public void startWaiting(){
        progressLogin.setVisibility(View.VISIBLE);
        btnSignin.setEnabled(false);
        btnLogin.setEnabled(false);

    }

    @Override
    public void stopWaiting(){
        progressLogin.setVisibility(View.GONE);
        btnSignin.setEnabled(true);
        btnLogin.setEnabled(true);


    }

}




