package com.grupo8.mydoctorhero.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.chip.Chip;
import com.grupo8.mydoctorhero.R;

public class CitizenRegActivity extends AppCompatActivity {

    private Chip endSigninCityzen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_ciudadano);


        endSigninCityzen = findViewById(R.id.endSigninCityzen);
        endSigninCityzen.setOnClickListener(v -> openProfileCitizen ());
    }
    public void openProfileCitizen(){

        Intent intent= new Intent( this, ActivityCivil1.class);
        startActivity(intent);
    }
}