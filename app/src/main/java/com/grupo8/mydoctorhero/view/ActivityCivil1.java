package com.grupo8.mydoctorhero.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.grupo8.mydoctorhero.R;

public class ActivityCivil1 extends AppCompatActivity {

    AppBarLayout tilAppBar;
    MaterialToolbar appBar;

    TextView txtRegEvento;

    TextInputLayout outlinedTextField11;
    TextInputEditText inpDescripcionEvento;

    TextInputLayout outlinedTextField22;
    TextInputEditText inpDetalleEvento;

    AppCompatButton btnAyuda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civil1);

        initUI();
    }

    private void initUI() {

        tilAppBar = findViewById(R.id.til_app_bar);
        appBar = findViewById(R.id.app_bar);
        /*appBar.setOnClickListener(evt ->onSettingsClick());*/

        txtRegEvento = findViewById(R.id.txt_reg_evento);

        outlinedTextField11 = findViewById(R.id.outlinedTextField1);
        inpDescripcionEvento = findViewById(R.id.inp_descripcion_evento);

        outlinedTextField22 = findViewById(R.id.outlinedTextField2);
        inpDetalleEvento = findViewById(R.id.inp_detalle_evento);

        btnAyuda = findViewById(R.id.btn_ayuda);
        btnAyuda.setOnClickListener(evt ->onAyudaClick());
    }

        private void onAyudaClick(){
            Intent intent = new Intent(this, ActivityCivil2.class);
            startActivity(intent);
        }

}