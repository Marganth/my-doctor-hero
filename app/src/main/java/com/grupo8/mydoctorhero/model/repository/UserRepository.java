package com.grupo8.mydoctorhero.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.grupo8.mydoctorhero.model.database.DoctorDatabase;
import com.grupo8.mydoctorhero.model.database.dao.UserDao;
import com.grupo8.mydoctorhero.model.database.entities.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance;

    public static UserRepository getInstance(Context context) {
        if (instance == null) {
            instance = new UserRepository(context);
        }
        return instance;
    }

    private final boolean USE_DATABASE = Boolean.FALSE;

    private UserDao userDao;

    // Write a message to the database

    private DatabaseReference userRef;


    private UserRepository(Context context) {
        userDao = DoctorDatabase.getDatabase(context).getUserDao();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("user");

        loadInitialData();
    }

    private void loadInitialData() {
        if (USE_DATABASE) {
            userDao.insertAll(
                    new User("Grupo 8", "grupo8@mintic.com", "12345678"),
                    new User("Test", "test@mintic.com", "87654321")
            );
        } else {
            String username = "grupo8@mintic.com".replace('@', '_').replace('.', '_');

            userRef.child(username).child("name").setValue("Grupo 8");
            userRef.child(username).child("email").setValue("grupo8@mintic.com");
            userRef.child(username).child("password").setValue("12345678");


            username = "test@mintic.com".replace('@', '_').replace('.', '_');
            userRef.child(username)
                    .setValue(new User("Usuario de Pruebas", "test@mintic.com", "87654321"));


        }


    }

    public void getUserByEmail(String email, UserCallback<User> callback) {
        if (USE_DATABASE) {
            callback.onSuccess(userDao.getUserByEmail(email));
        } else {
            //Usar Firebase
            // Read from the database
            String username = email.replace('@', '_').replace('.', '_');
            userRef.child(username).get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            User value = task.getResult().getValue(User.class);
                            Log.d(UserRepository.class.getSimpleName(), value.toString());
                            callback.onSuccess(value);
                        } else {
                            callback.onFailure();
                        }

                    });

        }
    }

    public void getAll(UserCallback<List<User>> callback) {
        userRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DataSnapshot dataSnapshot = task.getResult();
                if (dataSnapshot.hasChildren()) {
                    List<User> users = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        snapshot.child("name").getValue(String.class);
                        User user = snapshot.getValue(User.class);
                        Log.d(UserRepository.class.getSimpleName(), user.toString());
                        users.add(user);
                    }
                    callback.onSuccess(users);
                }else {
                    callback.onSuccess(new ArrayList<>());
                }
            } else {
                callback.onFailure();
            }
        });
    }


    public interface UserCallback<T> {
        void onSuccess(T data);

        void onFailure();
    }
}
