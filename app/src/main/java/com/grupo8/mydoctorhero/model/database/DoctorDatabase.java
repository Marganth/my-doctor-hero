package com.grupo8.mydoctorhero.model.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.grupo8.mydoctorhero.model.database.dao.UserDao;
import com.grupo8.mydoctorhero.model.database.entities.Role;
import com.grupo8.mydoctorhero.model.database.entities.User;


@Database(entities = {User.class, Role.class}, version = 1)
public abstract class DoctorDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();

    private  static  volatile DoctorDatabase INSTANCE;

    public static DoctorDatabase  getDatabase (Context context) {
        if (INSTANCE == null){
            INSTANCE = Room
                    .databaseBuilder(context.getApplicationContext(),DoctorDatabase.class, "database-name")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

}
