package com.grupo8.mydoctorhero.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.chip.Chip;
import com.grupo8.mydoctorhero.R;

public class ChoiceActivity extends AppCompatActivity {

    private Chip doctorChoice;
    private Chip citizenChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eleccion);


        doctorChoice = (Chip) findViewById(R.id.doctorChoice);
        doctorChoice.setOnClickListener(v -> doctorReg ());

        citizenChoice =(Chip) findViewById(R.id.cityzenChoice);
        citizenChoice.setOnClickListener(v -> citizenReg ());

    }
    public void doctorReg (){
        Intent intent = new Intent(this, HealthRegActivity.class);
        startActivity(intent);
    }
    public void citizenReg (){
        Intent intent = new Intent(this, CitizenRegActivity.class);
        startActivity(intent);
    }
}