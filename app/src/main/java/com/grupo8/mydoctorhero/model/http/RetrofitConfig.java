package com.grupo8.mydoctorhero.model.http;

import retrofit2.Retrofit;

public class RetrofitConfig {

    //Singleton
    private static RetrofitConfig instance;

    public static RetrofitConfig getInstance() {
        if(instance == null) {
            instance = new RetrofitConfig();
        }

        return instance;
    }
    private static Retrofit retrofit;

        private RetrofitConfig(){
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .build();
        }

        public  Retrofit getRetrofit(){
            return retrofit;
        }

}
