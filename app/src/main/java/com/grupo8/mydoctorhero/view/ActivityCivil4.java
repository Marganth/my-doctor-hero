package com.grupo8.mydoctorhero.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;


import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.grupo8.mydoctorhero.R;


public class ActivityCivil4 extends AppCompatActivity {

    AppBarLayout tilAppBar4;
    MaterialToolbar appBar4;
    TextView txtConfCancelar;
    Button btnSi;
    Button btnNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civil4);

        initUI();
    }

    private void initUI() {

        tilAppBar4 = findViewById(R.id.til_app_bar4);
        appBar4 = findViewById(R.id.app_bar4);
        appBar4.setOnClickListener(evt ->onSettingsClick4());

        txtConfCancelar = findViewById(R.id.txt_conf_cancelar);

        btnSi = findViewById(R.id.btn_si);
        btnSi.setOnClickListener(evt ->onSiClick());

        btnNo = findViewById(R.id.btn_no);
        btnNo.setOnClickListener(evt ->onNoClick());
    }
    private void onSettingsClick4(){
        Intent intent = new Intent(this, ActivityCivil2.class);
        startActivity(intent);
    }

    private void onSiClick(){
        Intent intent = new Intent(this, ActivityCivil5.class);
        startActivity(intent);
    }

    private void onNoClick(){
        Intent intent = new Intent(this, ActivityCivil3.class);
        startActivity(intent);
    }
}