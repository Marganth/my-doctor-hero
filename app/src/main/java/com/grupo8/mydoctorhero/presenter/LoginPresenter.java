package com.grupo8.mydoctorhero.presenter;

import com.grupo8.mydoctorhero.model.LoginInteractor;
import com.grupo8.mydoctorhero.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Model model;

     public LoginPresenter (LoginMVP.View view){
         this.view = view;
         this.model = new LoginInteractor(view.getActivity());
     }

    @Override
    public void loginWithEmail() {

         boolean error =false;


        view.showEmailerror("");
        view.showPassworderror("");


         LoginMVP.LoginInfo loginInfo = view.getLoginInfo();
         if (loginInfo.getEmail().trim().isEmpty()){
             view.showEmailerror("Correo electrónico es obligatorio");
         } else if(!isEmailValid(loginInfo.getEmail().trim())) {
             view.showEmailerror("Correo electrónico no es válido");
             error =true;

         }

         if (loginInfo.getPassword().trim().isEmpty()){
             view.showPassworderror("Contraseña es obligatoria");
         } else if (!isPaswordValid(loginInfo.getPassword().trim()))  {
             view.showPassworderror("Contraseña no cumple criterios de seguridad");
             error =true;
         }

         if (!error) {
            view.startWaiting();
             new Thread(()-> {
                 model.validateCredentials(loginInfo.getEmail().trim(),
                         loginInfo.getPassword().trim(),
                         new LoginMVP.Model.ValidateCredentialsCallback() {

                             @Override
                             public void onSuccess() {
                                 view.getActivity().runOnUiThread(() ->{
                                        view.stopWaiting() ;
                                        view.openNewActivity();
                                 });
                             }

                             @Override
                             public void onFailure(String error) {
                                view.getActivity().runOnUiThread(() -> {
                                    view.stopWaiting();
                                    view.showGeneralMessage(error);
                                });

                             }
                         });
             }).start();


         }
    }

    private boolean isEmailValid(String email) {
         return email.contains("@") && email.endsWith(".com");
    }

    private boolean isPaswordValid(String password) {
        return  password.length() > 7;
    }

    @Override
    public void loginWithFacebook() {

    }

    @Override
    public void loginWithGmail() {

    }

    @Override
    public void signin() {
        view.openSignin();


    }

}
