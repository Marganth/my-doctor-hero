package com.grupo8.mydoctorhero.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;


import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.grupo8.mydoctorhero.R;
import com.grupo8.mydoctorhero.view.ActivityCivil3;


public class ActivityCivil2 extends AppCompatActivity {

    AppBarLayout tilAppBar2;
    MaterialToolbar appBar2;
    TextView buscDocHero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civil2);

        initUI();
    }

    private void initUI() {

        tilAppBar2 = findViewById(R.id.til_app_bar2);
        appBar2 = findViewById(R.id.app_bar2);
        appBar2.setOnClickListener(evt ->onSettingsClick2());

        buscDocHero = findViewById(R.id.txt_buscando_doc_hero);
    }
        private void onSettingsClick2(){
            Intent intent = new Intent(this, ActivityCivil3.class);
            startActivity(intent);
        }

}