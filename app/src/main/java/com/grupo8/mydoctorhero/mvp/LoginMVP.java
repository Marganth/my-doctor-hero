package com.grupo8.mydoctorhero.mvp;


import android.app.Activity;
import android.content.Context;

public interface LoginMVP {

    interface Model{

        void validateCredentials(String email, String password, ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback {
            void onSuccess();
            void onFailure (String error);
        }
    }

    interface Presenter {
        void loginWithEmail();
        void loginWithFacebook();
        void loginWithGmail();
        void signin();




    }

    interface View{



        Activity getActivity();
        LoginInfo getLoginInfo();
        void showEmailerror(String error);
        void showPassworderror (String error);
        void showGeneralMessage(String error);


        void clearData();

        void openNewActivity();

        void openSignin();

        void startWaiting();

        void stopWaiting();


    }

    class LoginInfo{
        private String email;
        private String password;


        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }



        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
