package com.grupo8.mydoctorhero.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.grupo8.mydoctorhero.R;

import java.util.Objects;

public class Activity_Profesional_de_la_Salud2 extends AppCompatActivity {

    private Button cancelButton;
    private Button onMyWayButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesional_de_la_salud2);


        cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(v -> returnMain());

        onMyWayButton = findViewById(R.id.onMyWayButton);
        onMyWayButton.setOnClickListener(v -> goToNext());


    }

    public void returnMain() {
        Intent intent = new Intent(this, Activity_Profesional_de_la_Salud1.class);
        startActivity(intent);
    }

    public void goToNext() {
        Intent intent = new Intent(this, Activity_Profesional_de_la_Salud3.class);
        startActivity(intent);
    }

}