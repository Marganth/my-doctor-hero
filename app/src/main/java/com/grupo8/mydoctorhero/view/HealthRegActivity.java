package com.grupo8.mydoctorhero.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.chip.Chip;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.grupo8.mydoctorhero.R;
import com.grupo8.mydoctorhero.mvp.HealthRegMVP;
import com.grupo8.mydoctorhero.presenter.HealthRegPresenter;

public class HealthRegActivity extends AppCompatActivity implements HealthRegMVP.View {

    private Chip endSigninDoc;

    private TextInputLayout nameDoctor;
    private TextInputLayout emailDoctor;
    private TextInputLayout phoneDoctor;
    private TextInputLayout passDoctor;
    private TextInputLayout confirmPassDoctor;

    private TextInputEditText nameDoctor2;
    private TextInputEditText emailDoctor2;
    private TextInputEditText phoneDoctor2;
    private TextInputEditText passDoctor2;
    private TextInputEditText confirmPassDoctor2;

    private Button rethusButton;
    private Button pictureDoctorButton;


    private CheckBox checkBoxTerms;

    private ImageView photoDoctor;

    private HealthRegMVP.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_salud);


        presenter = new HealthRegPresenter(this);
        initUI();


    }

    private void initUI(){

        endSigninDoc = findViewById(R.id.endSigninDoc);
        endSigninDoc.setOnClickListener(v -> presenter.completeDoctorRegistration());


    }



    @Override
    public HealthRegMVP.RegDoctorInfo getDoctorInfo() {
        return null;
    }

    @Override
    public void showNameError(String error) {
        nameDoctor.setError(error);

    }

    @Override
    public void showEmailError(String error) {
        passDoctor.setError(error);
    }

    @Override
    public void showPhoneError(String error) {
        phoneDoctor.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        passDoctor.setError(error);
    }

    @Override
    public void showConfirmPasswordError(String error) {
        confirmPassDoctor.setError((error));
    }

    @Override
    public void showCheckError(String error) {
        checkBoxTerms.setError((error));
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(HealthRegActivity.this, "error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        nameDoctor.setError("");
        emailDoctor.setError("");
        phoneDoctor.setError("");
        passDoctor.setError("");
        confirmPassDoctor.setError("");
        checkBoxTerms.setError("");

        nameDoctor2.setError("");
        emailDoctor2.setError("");
        phoneDoctor2.setError("");
        passDoctor2.setError("");
        confirmPassDoctor2.setError("");



    }

    @Override
    public void openFinalProfile() {
        Intent intent = new Intent(this, Activity_Profesional_de_la_Salud1.class);
        startActivity(intent);

    }
}