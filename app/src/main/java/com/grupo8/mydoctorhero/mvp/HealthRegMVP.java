package com.grupo8.mydoctorhero.mvp;

import android.widget.CheckBox;

public interface HealthRegMVP {

    interface Model{

    }
    interface Presenter{
        void  completeDoctorRegistration();


    }
    interface View{
        RegDoctorInfo getDoctorInfo();
        void showNameError(String error);
        void showEmailError(String error);
        void showPhoneError(String error);
        void showPasswordError(String error);
        void showConfirmPasswordError(String error);
        void showCheckError(String error);
        void showGeneralError(String error);


        void clearData();

        void openFinalProfile();

    }

    class RegDoctorInfo {
        private String name;
        private String email;
        private String phone;
        private String password;
        private String confirmPassword;
        private CheckBox acceptTerms;


        public RegDoctorInfo(String name, String email, String phone, String password, String confirmPassword, CheckBox acceptTerms) {
            this.name = name;
            this.email = email;
            this.phone = phone;
            this.password = password;
            this.confirmPassword = confirmPassword;
            this.acceptTerms = acceptTerms;
        }



        public String getEmail() {
            return email;
        }

        public String getName() {
            return name;
        }

        public String getPhone() {
            return phone;
        }

        public String getPassword() {
            return password;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public CheckBox getAcceptTerms() {
            return acceptTerms;
        }
    }
}
