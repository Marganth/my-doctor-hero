package com.grupo8.mydoctorhero.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.grupo8.mydoctorhero.R;

public class Activity_Profesional_de_la_Salud1 extends AppCompatActivity {

    private Button option1;
    /*private Button option2;
    private Button option3;
    private Button option4;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesional_de_la_salud1);



        option1 = findViewById(R.id.option1);
        option1.setOnClickListener( v -> emergencyDetail());

    }
    public void emergencyDetail(){
        Intent intent = new Intent(this, Activity_Profesional_de_la_Salud2.class);
        startActivity(intent);

    }
}