package com.grupo8.mydoctorhero.model;

import android.content.Context;
import android.util.Log;

import com.grupo8.mydoctorhero.model.database.entities.User;
import com.grupo8.mydoctorhero.model.repository.UserRepository;
import com.grupo8.mydoctorhero.mvp.LoginMVP;

import java.util.List;

public class LoginInteractor implements LoginMVP.Model {


    private UserRepository userRepository;


    public  LoginInteractor(Context context) {
        userRepository =  UserRepository.getInstance(context);
    }


    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {
    userRepository.getUserByEmail(email, new UserRepository.UserCallback<User>() {
        @Override
        public void onSuccess(User user) {
            if (user == null){
                callback.onFailure("Usuario no existe");
            }else if (!user.getPassword().equals(password)){
                callback.onFailure("Contraseña incorrecta");
            } else {
                callback.onSuccess();
            }
        }

        @Override
        public void onFailure() {
            callback.onFailure("Error accediendo a fuente de datos");
        }
    });

    userRepository.getAll(new UserRepository.UserCallback<List<User>>() {
        @Override
        public void onSuccess(List<User> data) {
            for(User user : data){
                Log.d(LoginInteractor.class.getSimpleName(), user.toString());
            }
        }

        @Override
        public void onFailure() {
            Log.w(LoginInteractor.class.getSimpleName(), "Problemas al obtener datos");
        }
    });
    }
}