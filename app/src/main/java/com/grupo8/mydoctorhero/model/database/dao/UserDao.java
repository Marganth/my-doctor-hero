package com.grupo8.mydoctorhero.model.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.grupo8.mydoctorhero.model.database.entities.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query(" SELECT * FROM user")
    List<User> getAll();

    @Query(" SELECT * FROM user WHERE email = :email")
    User getUserByEmail (String email);

    @Insert
    void insertAll(User... users0);

    @Delete
    void delete (User user);


}
