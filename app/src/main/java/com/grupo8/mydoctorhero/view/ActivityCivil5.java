package com.grupo8.mydoctorhero.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;


import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.grupo8.mydoctorhero.R;


public class ActivityCivil5 extends AppCompatActivity {

    AppBarLayout tilAppBar5;
    MaterialToolbar appBar5;

    TextView txtRazonCancelarLlamado;

    RadioGroup radioGroup;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    RadioButton radioButton4;

    TextInputLayout textFieldActivityCivil5;
    TextInputEditText txtInpOtraRazon;

    Button btnCancelarLlamado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civil5);

        initUI();
    }

    private void initUI() {

        tilAppBar5 = findViewById(R.id.til_app_bar5);
        appBar5 = findViewById(R.id.app_bar5);
        appBar5.setOnClickListener(evt ->onSettingsClick5());

        txtRazonCancelarLlamado = findViewById(R.id.txt_razon_cancelar_llamado);

        radioGroup = findViewById(R.id.radioGroup);
        radioButton1 = findViewById(R.id.radio_button_1);
        radioButton2 = findViewById(R.id.radio_button_2);
        radioButton3 = findViewById(R.id.radio_button_3);
        radioButton4 = findViewById(R.id.radio_button_4);

        textFieldActivityCivil5 = findViewById(R.id.TextField_ActivityCivil5);
        txtInpOtraRazon = findViewById(R.id.txt_inp_otra_razon);

        btnCancelarLlamado = findViewById(R.id.btn_cancelar_llamado);
        btnCancelarLlamado.setOnClickListener(evt ->onCancelarLlamadoClick());
    }
    private void onSettingsClick5(){
        Intent intent = new Intent(this, ActivityCivil2.class);
        startActivity(intent);
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_button_1:
                if (checked)
                    // radio1
                    break;
            case R.id.radio_button_2:
                if (checked)
                    // radio2
                    break;
            case R.id.radio_button_3:
                if (checked)
                    // radio3
                    break;
            case R.id.radio_button_4:
                if (checked)
                    // radio4
                    break;
        }
    }

    private void onCancelarLlamadoClick(){
        Intent intent = new Intent(this, ActivityCivil1.class);
        startActivity(intent);
    }
}